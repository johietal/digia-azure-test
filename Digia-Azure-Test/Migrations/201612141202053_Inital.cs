namespace Digia_Azure_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Thread",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Url = c.String(nullable: false, maxLength: 450),
                        DateAdded = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Url, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Thread", new[] { "Url" });
            DropTable("dbo.Thread");
        }
    }
}
