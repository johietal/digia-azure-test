﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading;
using System.Web;

namespace Digia_Azure_Test.Models
{
    public class DigiaAzureContext : DbContext
    {
        public DigiaAzureContext() : base("DigiaAzureContext") { }

        public static DigiaAzureContext Create()
        {
            return new DigiaAzureContext();
        }

        public DbSet<Thread> SiteReferences { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}