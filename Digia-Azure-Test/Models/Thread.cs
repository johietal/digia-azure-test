﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digia_Azure_Test.Models
{
    public class Thread
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(450)]
        [Index("IX_Url", 1, IsUnique = true)]
        public string Url { get; set; }

        [Required]
        public DateTime DateAdded { get; private set; }

        public Thread()
        {
            DateAdded = DateTime.Now;

        }
    }
}